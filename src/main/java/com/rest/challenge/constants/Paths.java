package com.rest.challenge.constants;

public class Paths {

    public static final String VERSION1 = "/api/1.0";
    public static final String AUTHORIZED = "/auth";
    public static final String SIGNUP = "/signup";
    public static final String SIGNIN = "/signin";
    public static final String LOGIN = "/login";
    public static final String ADMIN = "/admin";
    public static final String USER = "/user";
    public static final String MOD = "/mod";
    public static final String EXTERNAL = "/external";
    public static final String SECRET = "zamtelAppSecretKey";
    public static final long EXPIRATION_TIME = 86_400_000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

}
