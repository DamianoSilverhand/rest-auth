package com.rest.challenge.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import static com.rest.challenge.constants.Paths.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(VERSION1)
public class TestController {
	@GetMapping(LOGIN)
	public String allAccess() {
		return "Login content can be accessed publicly.";
	}

	@GetMapping(USER)
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public String userAccess() {
		return "Content only accessible to User and Admin .";
	}

	@GetMapping(MOD)
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Access restricted to Moderators.";
	}

	@GetMapping(ADMIN)
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Only Content.";
	}

	@GetMapping(EXTERNAL)
	@PreAuthorize("hasRole('ADMIN')")
	public String externalAccess() {
		String uri = "https://apps.zamtel.co.zm/zamtelAssessment";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		return result;
	}


}
