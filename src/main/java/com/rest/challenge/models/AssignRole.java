package com.rest.challenge.models;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class AssignRole {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
	private EAssignRole name;

	public AssignRole() {

	}

	public AssignRole(EAssignRole name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EAssignRole getName() {
		return name;
	}

	public void setName(EAssignRole name) {
		this.name = name;
	}
}