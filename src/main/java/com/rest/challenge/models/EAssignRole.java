package com.rest.challenge.models;

public enum EAssignRole {
	ROLE_USER,
    ROLE_ADMIN,
    ROLE_MODERATOR
}
