package com.rest.challenge.repository;

import java.util.Optional;

import com.rest.challenge.models.AssignRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.challenge.models.EAssignRole;

@Repository
public interface RoleRepository extends JpaRepository<AssignRole, Long> {
	Optional<AssignRole> findByName(EAssignRole name);
}
