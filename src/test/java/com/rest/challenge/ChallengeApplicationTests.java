package com.rest.challenge;

import static com.rest.challenge.constants.Paths.*;
import static java.util.Collections.singletonList;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.StringUtils.collectionToDelimitedString;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith({ RestDocumentationExtension.class, SpringExtension.class })
@SpringBootTest(classes = ChallengeApplication.class)
public class ChallengeApplicationTests {

	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@BeforeEach
	public void setUp(WebApplicationContext webApplicationContext, RestDocumentationContextProvider restDocumentation) {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(documentationConfiguration(restDocumentation))
				.alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
				.build();
	}

	@Test
	public void returnLoginPage() throws Exception {
		this.mockMvc.perform(get(VERSION1 + LOGIN))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("Login content can be accessed publicly."))).andDo(document("login-page", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
				responseHeaders(headerWithName("Content-Type").description("`application/text`"))));
	}

	@Test
	public void createUser() throws Exception {
		Map<String, Object> user = new HashMap<>();
		user.put("username", "Mock2 User");
		user.put("email", "mock@user.com");
		user.put("password", "123456789");
		user.put("role", new String[]{"user", "admin"});


		this.mockMvc.perform(post(VERSION1 + AUTHORIZED + SIGNUP).contentType(MediaTypes.HAL_JSON)
				.content(this.objectMapper.writeValueAsString(user)))
//				.andExpect(status().isOk())
//				.andExpect(content().string(containsString( "User registered successfully!")))
//				.andDo(document("create-user", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), requestFields(fieldWithPath("username").description("The username of the input"), fieldWithPath("email").description("The email of the input"),
//						fieldWithPath("password").description("The password of the input"), fieldWithPath("roles").description("An array of roles"))))
				.andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("Error: Email is already in use!"))).andDo(document("create-user", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
						responseHeaders(headerWithName("Content-Type").description("`application/text`"))));
	}

	@Test
	public void loginUser() throws Exception {
		Map<String, Object> user = new HashMap<>();
		user.put("username", "Mock User");
		user.put("password", "123456789");


		this.mockMvc.perform(post(VERSION1 + AUTHORIZED + SIGNUP).contentType(MediaTypes.HAL_JSON)
				.content(this.objectMapper.writeValueAsString(user)))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString( "User registered successfully!")))
				.andDo(document("create-user", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), requestFields(fieldWithPath("username").description("The username of the input"), fieldWithPath("email").description("The email of the input"),
						fieldWithPath("password").description("The password of the input"), fieldWithPath("roles").description("An array of roles"))))
				.andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("Error: Email is already in use!"))).andDo(document("create-user", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()),
				responseHeaders(headerWithName("Content-Type").description("`application/text`"))));
	}


	@Test
	public void contextLoads() {
	}

}

